# Installing and using mkdocs

Steps to setup a new Repup documentation enabled system

1. Install pip and mkdocs (https://docs.readthedocs.io/en/stable/intro/getting-started-with-mkdocs.html)
2. Create and configure bitbucket account and get access
3. Clone current bitbucket repository for parent documention
4. Install Typora tool to edit documentation (https://typora.io)
5. Open project folder in Typora
6. Edit in Typora save it and publish to bitbucket
7. As you publish your documentation change is live

Above are mandatory steps to be taken to actually work with parent documentations



See more:

[About writing your docs](https://mkdocs.readthedocs.io/en/stable/user-guide/writing-your-docs/)

[About Mkdocs](https://docs.readthedocs.io/en/stable/intro/getting-started-with-mkdocs.html)



