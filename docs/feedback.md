##Room Number to Guest Details


###For post-stay form

####When we are getting reservation details after check-out only

1. Check for check-out on the current date for entered room number and show details accordingly

####When we are getting reservation details just after booking

1. No check-out exists on the current date:
	- If someone is in-house in the room, show details for that guest
	- If someone is not in-house in the room, show blank

2. Check-out exists on current date:
	- Show details of guest with check-out on current date


###For in-stay form

###When we are getting reservation details after check-out only

1. Check for check-out on the current date for entered room number and show details accordingly

###When we are getting reservation details after booking

1. No check-out exists on the current date:
	- If someone is in-house in the room, show details for that guest
	- If someone is not in-house in the room, show blank

2. Check-out exists on current date:
	- If someone is not staying in the room currently, show details of the guest with check-out on current date (provided state is not processed)
	- If someone is staying in the room currently, show details of that guest