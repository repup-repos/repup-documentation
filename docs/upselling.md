Upsells are 'upgrade offers' that hotels can promote to their guests through emails and mobile concierge. Upsells can include Room Upgrades, Spa offers, Tours & Excursions, F&B offers, Hotel Transfers, Events, Vehicle Rentals and other additional services. This document covers how upsells are created and managed in Xperium.

## Creating Upsells

Upsell offers can be created with these required details:

1. **Offer Title**
2. **Offer Tags**: Tags describing the upsell e.g. 30-min session, hatha yoga, rejuvenate, relax etc
3. **Offer Description**: Offer description and details
4. **Offer Image(s)**: One or more images for the upgrade offer need to be added to be shown in upsell card
5. **Upsell Category**: The category that upsell belongs to. Below are the available Categories
	* 	Room Upgrades
	* 	Food & Beverage Offers
	*  Hotel Transfers
	*  Spa & Wellness Offers
	*  Events
	*  Tours & Excursions
	*  Vehicle Rentals
	*  Additional Services
	*  Yoga Activites

6. **Upsell Details**: Upsell details can be entered in this section using a WYSWIG editor which can be viewed using **view details** option on the upsell

7. **Offer Types**: Offer types define how upsell requests would be awarded to the guest

	* **Auto Accept**: These offers would be accepted automatically and the guest will be sent a confirmation email instantly upon requesting such upsells
	* 	**Require Confirmation**: These offers would require confirmation from the property. Upsells can be accepted or declined by the property. The guest will get a confirmation or a cancellation email accordingly.

8. **Price Model**: This defines on what basis the upsell offer is priced

	- **Single Price**: Pricing model for all upsells with a fixed price and only one time purchase applicable.
	- **Price per Unit**: Pricing model for upsells that can be bought in multiple quantities. One can limit the maximum number of units guest can buy.
	- **Price per Person**:  Pricing model for upsells that are priced per person. One can limit the maximum number of people who can buy.
	- **Price per Night**: Pricing model for upsells that are priced per night. Nights are automatically picked from the reservation.
	- **Discount Offer**: All upsells that are just discount offers and a customer who avails the offer wouldn't need to pay anything to get that offer.
	- **Free**: Offers that are completely free of cost.

9. **Price Variables**: Upsells can be at a **fixed price**, or the price can be varied on the basis of **Room Category** and **Rate Plan**
10. **Guest Stage**: This defines the stages at which upsells would be available. Upsells can be available in one of the stages or both stages
	- **Pre-Stay**: Upsell Offers would only be available before check-in
	- **In Stay**: Upsell offers would only be available after check-in
11. **Multiple Products in an upsell**: An upsell can constitute of multiple products. e.g. A spa upsell can have 30-min, 60-min and 90-min massage; each priced differently at a fixed price, or variable price according to room category or rate plan.

	These products can have their own images uploaded and would be selected by the guest using a dropdown.



## **Other details while Creating an Upsell**

1. Upsell preview is available in real time as the upsell is edited.
2. **Offer Terms**: This section can be used to list any terms and conditions associated with the upsell offer
3. **Offer Timings**: This section can be used to state offer timings, e.g tour availability timings, spa availability timings etc.
4. **Offer Notifications**: This section controls which departments should the notifications be sent to
5. **Reservation Details**: Details that can be asked from the guest when buying/requesting the upsell
	- **People**: Number of people for which the guest wants to request upsell offer
	- **Date**: Date on which upsell offer needs to be availed
	- **Time**: Time at which upsell offer needs to be availed
	- **Flight Details**: Flight Number details
	
6. **Offer Price and Regular Price**: Discounted price and regular price for the upsell offer
7. **Available for Room types**: This section controls the reservations for which the upsell would be visible, based on the room category they belong to
8. **Available for Rate Plans**: This section controls the reservations for which the upsell would be visible, based on the rate plan they belong to
9. **Not available if upsells bought**: This section hides the upsell being edited for guests who have bought a particular upsell
8. **Nights Available**: Nights on which the upsell offer would be visible (by day of the week)
9. **Inventory**: Total inventory (number) of upsell offers available