# Welcome to Repup Documentation



> Repup public documentation includes
>
> 1. Marketing campaigns
> 2. Scheduled campaigns
> 3. Release notes

# Marketing Campaigns

Repup marketing module complete feature set to drive all kind of marketing campaigns. User can use dynamically updated lists sourced directly from their PMS/Feedback Surveys/Uploaded database

[Read more...](marketing-campaigns)



# Booking Suite

Booking suite App Store and In App Integration is done to connect property booking accounts with Repup. This helps in connecting to their reservations as well as reviews, hotels data. For further details [click here](booking-suite.md)



# Release Logs

To view all release logs 

[click here](releaselogs.md)



This log is being created by mkdocs to learn more [click here](tutorials.md)