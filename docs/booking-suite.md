# Booking Suite Integration

Identification for booking suite client and Repup client is done basis on connection_id as API's are not clear about it. So we have to use combination of both of them to just confirm about what is an integration issue

## App Store:

App store booking suite integration 

1. User will be redirected to connect screen 

2. Connect screen will have handle the request

   

   1. In case of invalid code and incomplete request from booking suite will ask to reconnect button to booking suite

   2. In case of valid code setup process will start

      

      1. Setup process checks for any existing client associated with current request and setup for property is already done it will redirect directly to mention account 

      2. In case existing client connection exists but property setup is not there. It will create a property add to existing client and auto signup with mentioned user. If user not exists in will create one and login with same 
      3. In case no previous client connection exists. It will create one and add property to given account and user can navigate to further screens for configuration
   
      

## Checks to setup a new account

Booking suite account will be created for single property account in combination with user information. As group accounts are not supported yet. We will be creating a new account based on Booking user_id, hotel connection_id. With unique combination of Booking user_id and hotel connection_id accounts will be created. There is possibility for having multiple accounts per property as multiple user can intiate multiple connection. This remains as is till we have valid support for enterprise accounts







