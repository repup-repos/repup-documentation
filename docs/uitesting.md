# Repup Testing

#### Feedback 

- Create Feedback  
- Survey Language 
- Feedback conversion stats
- Survey Analysis 
- Reviews Feed 
- Review Reply 
- Tripadvisor / google survey creation  
- Conversion Stats
- User-wise notification setting

#### PMS Communication

- Add new reservation

- Guest Info display

- Mobile Conceirge/ Web-Checkin/ Review invite emails

- Request and upsell webcheckin visibility

- "Mobile Conceirge"/ "Web-Checkin"/ "Review invite" campaign stats 


#### Marketing

- Create new marketing email (Multi list/Single list/ Search dynamic list)

-  Uploaded list

- Check marketing stats (Number's and deliveries)

- Booking review tracker --?

- Multi CTA check

- Marketing stats (Download)

- Unsubscribe 

  - send a campaign (eg. to List-1 )--> unsubscribe from email sent (let say xyz@abc.com has subscribed) --> resend a campaign to the same list --> check if the email is still going or not

  - In continuation to to last test case --> send a new campaign to List-1 and List-2(also has xyz@abc.com but the status is subscribed)  --> check if the email is still going or not

  - Unsubscribe from the dashboard and than check the 

    

#### Automated Emailing

- Configure new automated emails using "staymx search list"/"feedback search" list/"custom list"
- Change active automated campaigns
- Automated campaigns stats 
- Automated "test campaign" 

#### Partner Testing

- Booking Signup and processing 
- Booking login and auto crawling
- Hotelogix Singup 
- Hotelogix login and permissions settings

#### ORM/Admin

- Change privileges settings from admin and verify in ORM
- Change screen display settings from admin and verify in ORM
- Filters testing 
- Report Generation 
- Change User privileges from dashboard and verify accessibility



 