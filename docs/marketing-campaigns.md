## Creating Lists

Lists can be created in two ways

1. **CSV Upload**

   Any data can be uploaded in a format provided to create lists. The following headers are allowed:

   Email, First Name, Last Name, City, Country, Birthday, Anniversary, Gender, Check-in Date, Check-out Date, Room Type, Adults, Children, Booking Source, Guest Type, Room No

    

2. **Guest Database Search**

   - **All Guest Data**: Search All Geusts Data coming from PMS (This constitues of all reservations/bookings): Booking Source, Check-in Date, Check-out Date, Adult Count, Child Count, Booking Confirmation #, Room #, Reservation State, City, Country, Email, Name, Phone Number, State, Room Category, Nights Stayed, Total Revenue, Anniversary, Birthday, Preferences (as configured)
   - **Feedback Data**: Search Data of Guests who have completed feedback form: Individual Ratings
   - **Direct Upload**: Search from lists uploaded from CSVs (Depending on which fields were selected at the time of upload):  Email, First Name, Last Name, City, Country, Birthday, Anniversary, Gender, Check-in Date, Check-out Date, Room Type, Adults, Children, Booking Source, Guest Type, Room No

   

   **Note**: created from Database can be chosen to update dynamically based on the search criteria. For example, if a list search criteria is all Guests whose Booking source is Booking.com, the list will keep updating as new guests make a booking from Booking.com

## List Upload/Creation Process

Lists uploaded as CSVs go through an approval process to check for bounced and invalid entries. Bounces are checked from **neverbounce.com** and any lists with more than 5% est. bounce rate are automatically rejected.

Once lists pass the bounce test, they need to be manually approved before they can be used for sending campaigns.

## Creating Campaigns

Campaigns are created and sent on pre-uploaded lists. The process works as follows:

1. **Campaign Name and List Selection**: A unique campaign name has to be assigned to the campaign (this is not visible to contacts receiving the campaign) and one or more lists have to be selected to send the campaign to
2. **Select Template**: A template can be selected from pre-defined templates, or a new design can be created from scratch
3. **Email Design**: Emails can be designed by a simple drag and drop system, one can insert text, images, sections (single and multi-column), social icons etc. The email editor is provided by [BEE editor](https://beefree.io/bee-plugin/)
4. **Email Subject, Sender Name etc**: Email subject has to be entered in this step, and a sender name and email can be chosen based on what has been configured already. One can also choose a reply-to email address and email addresses to bcc the email.
5. **Final Review**: In this screen one can check the campaign details like list, subject, preview etc before sending the campaign
6. **Send or Schedule**: The campaign can either be sent instantly or scheduled for a later time/date.


## **New Search System**

1. Testing of the system for basic architectural alignment
2. Implemented Lock functionality for concurrent/parallel processing. Avoids loss of data and unnecessary collisions.
3. Optimised the search codebase which included
	* Recurring queries(reduced duplicate queries by amount of 7 to zero)
	* Handling of data irrespective of the source they are coming from(Script, Webhook) 
4. Testing of the system for data consistency

##Templates
Templates are pre-available email templates that can be used to create emails. New templates upto 100 can be created for each property.
Marketing->Configuration->Templates

##From Emails
From emails are configured emails that can be used to send emails from. e.g. if xhotels want to send emails from xhotels.com domain, they can configure an email like contact@xhotels.com or hello@xhotels.com by adding the email in configuration section and confirming the email by clicking on verification email recieved.
Marketing->Configuration->From Emails

## Unsubscribe

Unsubscription can happen in two ways:

1. **Promotional Email Unsubscription**: All Emails that are classified as Promotional (All Marketing Campaigns, Birthday Campaigns, Anniversary Campaigns, Bringback Emails)
2. **Transactional Email Unsubscription**: All Emails that are qualified as Transactional (Booking Confirmation, Feedback Invite, Pre-arrival welcome, check-in confirmation, Review Invite)

A contact can be unsubscribed in two ways:

1. **Email Unsubscribe**: An unsubscribe link is embedded in every email that goes out to guests. They can click on the link and choose to end subscription from either promotional emails, or transactional emails or both.
2. **Manual Unsubscribe**: An Xperium user can manually choose to unsubscribe an email (from a list) or CRM Guest from either promotional emails, or transactional emails or both.

Global Unsubscribers List:

1. **Promotional Email Unsubscribers List**: All Email Addresses that have unsubscribed from promotional emails either from an email or manually by a user

2. **Transactional Email Unsubscribers List**: All Email Addresses that have unsubscribed from transactional emails either from an email or manually by a user

   An Email address can exist in both unsubscriber lists at the same time. In such case, no email should be sent to the user from RepUp system unless it is re-subscribed.

**How will unsubscribed entries be handled**

While sending a campaign, all of its contacts would be matched against promotional or transactional unsubscribers list based on the type of campaign, and the unsubscribes identified. This number will be sent to deliveries with unsubscribe tag for stats calculation.

## Archive Lists & Campaigns

**Archived Lists**: Any search based or uploaded lists can be archived. Such archived lists cannot be reused for sending campaigns.

**Archived Campaigns**: Any campaigns in draft or previously sent can be archived. Such campaigns cannot be reused, duplicated or edited. However, stats would be available for any campaigns that have been sent previously.

**Sending Campaigns to Archived Lists**

1. Archived lists would not be available for selection at the time of sending a campaign. 
2. If a campaign saved as draft or a duplicate campaign contains an archived list, the campaign cannot be sent without removing that list and selecting some other list
3. If a campaign is scheduled to be sent at a particular time and contains an archived list, it would be sent anyway.
