Below is a list of checkpoints that need to be gone through before making changes live



## **Checkpoints for automated campaigns**

1. Campaigns work according to defined Schedule.
2. Dependency on schedule defaults(it creates error sometimes on edge cases)
3. No testing infra is set up to ensure expected behaviour
4. Can ensure successful behaviour by up to 90%. Can ensure 100% with automated unit testing. It can catch potential failures before they happen.
5. Celery monitoring is a crucial part to ensure deliverability
	* No way to determine overflow of resources( efficiency with respect to cost)
	* Current calculations of usage of resources aren’t reliable
	* Lengthy to debug because of work done by multiple people on the development pipeline. It takes time to digest unknown configurations before solving a potential problem.


## **Checkpoints for Revenue**

1. No unit testing to ensure correct calculations
2. Normal data consistency monitoring by hand currently. Not efficient and there is scope of errors.
3. Can ensure 90% efficiency currently. 



## **Checkpoints for Search**
1. No Unit Test to ensure handling of different data templates processing
2. No Data consistency report as of now.
3. Data is big and covering all the past data of Repup product.
4. With manual observation, 60% consistency can be ensured
5. And the working of the code can be claimed to have 90% correct. Only after unit testing 100% surity can be given.

