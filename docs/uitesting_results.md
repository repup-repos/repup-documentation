# Repup Testing

#### Feedback 

- Create Feedback  --> "Tags" are not getting added for Single and Group rating questions
- Survey Language
- Feedback conversion stats
- Survey Analysis  
- Reviews Feed 
- Review Reply --> issue 
- Tripadvisor / google survey creation  

#### PMS Communication

- Add new reservation

- Guest Info display

- Mobile Concierge/ Web-Checkin/ Review invite emails :

  **Observations:**

  - if a checked-out(let say previous day check-out)  guest info is addedeven than also web check-in email is going  
  - Mobile Concierge emails not going

- Request and upsell webcheckin visibility

- "Mobile Conceirge"/ "Web-Checkin"/ "Review invite" campaign stats 

  - **Observation:** Not opening

#### Marketing

- Create new marketing email (Multi list/Single list/ Uploaded list/ Search dynamic list)

- Upload List:

  - "Add to List" not working

  **Observations:**

  - Upload List: List is not visible under Marketing/Marketing Campaigns/Lists but is visible when creating the campaign

- Check marketing stats (Number's and deliveries)

  **Observations:**

  - Stats are incorrect. 

- Booking review tracker 

- Multi CTA check

- Marketing stats (Download)  --> not downloading 

### Upselling

- Create upsell 
- Upsell request notification to guest
- Upsell request notifications to the client --> incorrect email id is going in the notification
- Upsell bought report

#### Automated Emailing

- Configure new automated emails :

  - -->Date based campaign scheduling on custom list is not possible, eg. birthday/anniversary campaigns
  - -->stats not loading
  - -->"TEST CAMPAIGN" not working

- Change active automated campaigns

- Automated campaigns stats : --> Not loading

  

#### Partner Testing

- Booking Signup and processing 
- Booking login and auto crawling
- Hotelogix Singup 
- Hotelogix login and permissions settings

#### ORM/Admin

- Change priviledges settings from admin and verify in ORM --> not working 
- Change screen display settings from admin and verify in ORM  --> not working
- Filters testing --> working fine
- Report Generation : Report History is not working/ reports not getting generated for the period other than "month"filter
- Change User privileges from dashboard and verify accessibility --> Generated Report, manage social account, integrations & CRM are still accessible to all the users



 